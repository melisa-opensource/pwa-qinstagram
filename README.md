# QInstagram

## Introduction HTML5
1. Basic tags
    - Tag `<p>`
    - Tag `<i>`
    - Tag `<b>`
    - Tag `<h1>`
    - Tag `<img>`
    - Tag `<a>`
1. Applying style
    - Property style
    - Tag `<style>`
    - Property class
    - Property id
1. Forms
    - Tag `<input type="text">`
    - Tag `<input type="email">`
    - Tag `<input type="password">`
    - Tag `<input type="checkbox">`
    - Tag `<select>`
    - Tag `<button>`
1. JavaScript
    - Tag `<script>`
1. Reference
    - [Tags](https://www.w3schools.com/tags/tag_section.asp)

